package com.pachoyan.itinerary;

import java.time.LocalDate;
import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public interface Itineraries {

    Collection<Itinerary> all();

    default Stream<Itinerary> stream() {
        return all().stream();
    }

    Collection<Itinerary> findFor(String city);

    Collection<Itinerary> findFor(String city, String destiny);

    Collection<Itinerary> findFor(String city, LocalDate date);

    Collection<Itinerary> findFor(String city, String destiny, LocalDate date);



}
