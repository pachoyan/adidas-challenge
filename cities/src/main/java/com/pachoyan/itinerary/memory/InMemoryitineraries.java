package com.pachoyan.itinerary.memory;

import com.pachoyan.itinerary.Itineraries;
import com.pachoyan.itinerary.Itinerary;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class InMemoryItineraries implements Itineraries {

    private final Collection<Itinerary> itineraries;

    public InMemoryItineraries(Collection<Itinerary> itineraries) {
        this.itineraries = itineraries;
    }

    @Override
    public Collection<Itinerary> all() {
        return itineraries;
    }


    @Override
    public Collection<Itinerary> findFor(String city) {
        return stream()
                .filter(itinerary -> city.equals(itinerary.getCity()))
                .collect(Collectors.toList());
    }

    @Override
    public Collection<Itinerary> findFor(String city, LocalDate date) {
        return findFor(city).stream()
                .filter(itinerary -> date.equals(itinerary.getDeparture().toLocalDate()))
                .collect(Collectors.toList());
    }

    @Override
    public Collection<Itinerary> findFor(String city, String destiny) {
        return findFor(city).stream()
                .filter(itinerary -> destiny.equals(itinerary.getDestiny()))
                .collect(Collectors.toList());
    }

    @Override
    public Collection<Itinerary> findFor(String city, String destiny, LocalDate date) {
        return findFor(city, date).stream()
                .filter(itinerary -> destiny.equals(itinerary.getDestiny()))
                .collect(Collectors.toList());
    }
}
