package com.pachoyan.itinerary;

import java.time.OffsetDateTime;
import java.util.Objects;

public class Itinerary {

    private final String city;
    private final String destiny;
    private final OffsetDateTime departure;
    private final OffsetDateTime arrival;


    public Itinerary(String city, String destiny, OffsetDateTime departure, OffsetDateTime arrival) {
        this.city = city;
        this.destiny = destiny;
        this.departure = departure;
        this.arrival = arrival;
    }

    public String getCity() {
        return city;
    }

    public String getDestiny() {
        return destiny;
    }

    public OffsetDateTime getDeparture() {
        return departure;
    }

    public OffsetDateTime getArrival() {
        return arrival;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Itinerary itinerary = (Itinerary) o;
        return Objects.equals(city, itinerary.city) &&
                Objects.equals(destiny, itinerary.destiny) &&
                Objects.equals(departure, itinerary.departure) &&
                Objects.equals(arrival, itinerary.arrival);
    }

    @Override
    public int hashCode() {

        return Objects.hash(city, destiny, departure, arrival);
    }
}
