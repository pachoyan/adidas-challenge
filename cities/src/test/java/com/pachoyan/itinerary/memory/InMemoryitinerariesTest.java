package com.pachoyan.itinerary.memory;

import com.pachoyan.itinerary.Itinerary;
import org.junit.Test;

import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;

public class InMemoryItinerariesTest {

    @Test
    public void shouldGetAllTheItineraries() {
        //given
        final Itinerary madridToBarcelona = new Itinerary("madrid", "barcelona", OffsetDateTime.now(), OffsetDateTime.now().plusHours(1));
        final Itinerary barcelonaToLondon = new Itinerary("barcelona", "london", OffsetDateTime.now(), OffsetDateTime.now().plusHours(2));
        final Itinerary madridToBerlin = new Itinerary("madrid", "berlin", OffsetDateTime.now(), OffsetDateTime.now().plusHours(3));
        final List<Itinerary> itineraryList = asList(madridToBarcelona, barcelonaToLondon, madridToBerlin);
        final InMemoryItineraries itineraries = new InMemoryItineraries(itineraryList);

        //when
        final Collection<Itinerary> actual = itineraries.all();

        //then
        assertEquals(actual, itineraryList);
    }


    @Test
    public void shouldGetItinerariesForCityAndDate() {
        //given
        final Itinerary madridToBarcelona = new Itinerary("madrid", "barcelona", OffsetDateTime.now(), OffsetDateTime.now().plusHours(1));
        final Itinerary madridToBarcelonaTomorrow = new Itinerary("madrid", "barcelona", OffsetDateTime.now().plusDays(1), OffsetDateTime.now().plusDays(1).plusHours(1));
        final Itinerary barcelonaToLondon = new Itinerary("barcelona", "london", OffsetDateTime.now(), OffsetDateTime.now().plusHours(2));
        final Itinerary madridToBerlin = new Itinerary("madrid", "berlin", OffsetDateTime.now(), OffsetDateTime.now().plusHours(3));
        final List<Itinerary> itineraryList = asList(madridToBarcelona, madridToBarcelonaTomorrow, barcelonaToLondon, madridToBerlin);
        final InMemoryItineraries itineraries = new InMemoryItineraries(itineraryList);

        //when
        final Collection<Itinerary> actual = itineraries.findFor("madrid", LocalDate.now());

        //then
        assertEquals(actual, asList(madridToBarcelona, madridToBerlin));
    }

    @Test
    public void shouldGetItinerariesForCityAndDestiny() {
        //given
        final Itinerary madridToBarcelona = new Itinerary("madrid", "barcelona", OffsetDateTime.now(), OffsetDateTime.now().plusHours(1));
        final Itinerary madridToBarcelonaTomorrow = new Itinerary("madrid", "barcelona", OffsetDateTime.now().plusDays(1), OffsetDateTime.now().plusDays(1).plusHours(1));
        final Itinerary barcelonaToLondon = new Itinerary("barcelona", "london", OffsetDateTime.now(), OffsetDateTime.now().plusHours(2));
        final Itinerary madridToBerlin = new Itinerary("madrid", "berlin", OffsetDateTime.now(), OffsetDateTime.now().plusHours(3));
        final List<Itinerary> itineraryList = asList(madridToBarcelona, madridToBarcelonaTomorrow, barcelonaToLondon, madridToBerlin);
        final InMemoryItineraries itineraries = new InMemoryItineraries(itineraryList);

        //when
        final Collection<Itinerary> actual = itineraries.findFor("madrid", "barcelona");

        //then
        assertEquals(actual, asList(madridToBarcelona, madridToBarcelonaTomorrow));
    }

    @Test
    public void shouldGetItinerariesForCity() {
        //given
        final Itinerary madridToBarcelona = new Itinerary("madrid", "barcelona", OffsetDateTime.now(), OffsetDateTime.now().plusHours(1));
        final Itinerary madridToBarcelonaTomorrow = new Itinerary("madrid", "barcelona", OffsetDateTime.now().plusDays(1), OffsetDateTime.now().plusDays(1).plusHours(1));
        final Itinerary barcelonaToLondon = new Itinerary("barcelona", "london", OffsetDateTime.now(), OffsetDateTime.now().plusHours(2));
        final Itinerary madridToBerlin = new Itinerary("madrid", "berlin", OffsetDateTime.now(), OffsetDateTime.now().plusHours(3));
        final List<Itinerary> itineraryList = asList(madridToBarcelona, madridToBarcelonaTomorrow, barcelonaToLondon, madridToBerlin);
        final InMemoryItineraries itineraries = new InMemoryItineraries(itineraryList);

        //when
        final Collection<Itinerary> actual = itineraries.findFor("madrid");

        //then
        assertEquals(actual, asList(madridToBarcelona, madridToBarcelonaTomorrow, madridToBerlin));
    }



    @Test
    public void shouldGetItinerariesForCityAndDestinyAndDate() {
        //given
        final Itinerary madridToBarcelonaToday = new Itinerary("madrid", "barcelona", OffsetDateTime.now(), OffsetDateTime.now().plusHours(1));
        final Itinerary madridToBarcelonaTomorrow = new Itinerary("madrid", "barcelona", OffsetDateTime.now().plusDays(1), OffsetDateTime.now().plusDays(1).plusHours(1));
        final Itinerary barcelonaToLondon = new Itinerary("barcelona", "london", OffsetDateTime.now(), OffsetDateTime.now().plusHours(2));
        final Itinerary madridToBerlin = new Itinerary("madrid", "berlin", OffsetDateTime.now(), OffsetDateTime.now().plusHours(3));
        final List<Itinerary> itineraryList = asList(madridToBarcelonaToday, madridToBarcelonaTomorrow, barcelonaToLondon, madridToBerlin);
        final InMemoryItineraries itineraries = new InMemoryItineraries(itineraryList);

        //when
        final Collection<Itinerary> actual = itineraries.findFor("madrid", "barcelona", LocalDate.now());

        //then
        assertEquals(actual, Collections.singletonList(madridToBarcelonaToday));
    }
}
