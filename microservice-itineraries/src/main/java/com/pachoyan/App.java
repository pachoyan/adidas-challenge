package com.pachoyan;

import com.pachoyan.itinerary.Itineraries;
import com.pachoyan.itinerary.Itinerary;
import com.pachoyan.itinerary.memory.InMemoryitineraries;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
@EnableDiscoveryClient
public class App {

    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }

    @Bean
    public Itineraries itineraries() {
        //replace with a sql, NoSql or whatever
        final List<Itinerary> itineraries = new ArrayList<>();
        itineraries.add(new Itinerary("madrid", "barcelona", OffsetDateTime.now(), OffsetDateTime.now().plusHours(1)));
        itineraries.add(new Itinerary("madrid", "alicante", OffsetDateTime.now(), OffsetDateTime.now().plusHours(1)));
        itineraries.add(new Itinerary("alicante", "london", OffsetDateTime.now(), OffsetDateTime.now().plusHours(3)));
        itineraries.add(new Itinerary("barcelona", "london", OffsetDateTime.now(), OffsetDateTime.now().plusHours(2)));

        return new InMemoryitineraries(itineraries);
    }
}
