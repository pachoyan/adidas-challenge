package com.pachoyan.controller;

import com.pachoyan.itinerary.Itineraries;
import com.pachoyan.itinerary.Itinerary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import java.time.LocalDate;
import java.util.Collection;

@Controller
public class ItinerariesController {

    private final Itineraries itineraries;

    @Autowired
    public ItinerariesController(Itineraries itineraries) {
        this.itineraries = itineraries;
    }

    @GetMapping("/itineraries")
    public @ResponseBody
    Collection<Itinerary> all() {
        return itineraries.all();
    }

    @GetMapping("/itineraries/{city}")
    public @ResponseBody
    Collection<Itinerary> findFor(@PathVariable("city") String city) {
        return itineraries.findFor(city, LocalDate.now());
    }
}
